import json
import requests

name = "infabula"
#url = "https://api.github.com/repos/kennethreitz"
#url = "https://api.github.com/users/kennethreitz/repos"
url = "https://api.github.com/users/" + name + "/repos"


session = requests.session()
result =  session.get(url)
if result.status_code == 200: # success
    data = json.loads(result.text) # deserialize
    for repo in data:
        print("{}".format(repo['name']))
else:
    print("error getting quote")
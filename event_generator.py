''' Event Generator '''
import datetime
import random

def event_generator(start_time, minimum, maximum):
    ''' Generatates random times '''
    event_time = start_time
    while True:
        yield event_time
        rand_seconds = random.randint(minimum*60, maximum*60)
        dt = datetime.timedelta(seconds=rand_seconds)
        event_time += dt

if __name__ == "__main__":
    now = datetime.datetime.now()
    run_time = 120 # minutes
    stop_time = now + datetime.timedelta(minutes=run_time)
    for event in event_generator(now, 2, 7):
        if event >= stop_time:
            break
        print(event)
